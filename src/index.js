import express from 'express';
import cors from 'cors';

import canonize from './canonize';
import canonizeColor from './canonizeColor';
import blackBox from './blackbox';

function sum(a = 0, b = 0) {
  return (a * 1) + (b * 1);
}

function parseFullname(fullname = '') {
  if (fullname.length < 1) {
    return 'Invalid fullname';
  }

  const redig = /([0-9_.+()^[\]/-]+)/gi;
  const digits = fullname.match(redig);

  if (digits != null) {
    return 'Invalid fullname';
  }

  const re = /([a-zа-яёó]+)/gi;
  const names = fullname.match(re);

  const num = names.length;

  if (num < 1 || num > 3) {
    return 'Invalid fullname';
  }
  let surename = names[num - 1];
  if (surename.length > 1) {
    surename = surename[0].toUpperCase() + surename.substring(1).toLowerCase();
  } else {
    surename = surename.toUpperCase();
  }
  let shortname = surename;
  for (let i = 0; i < (num - 1); i += 1) {
    const letter = names[i][0];
    shortname = `${shortname} ${letter.toUpperCase()}.`;
  }
  return shortname;
}

const app = express();
app.use(cors());

app.get('/task2A/', (req, res) => {
  const s = sum(req.query.a, req.query.b);
  res.send(s.toString());
});

app.get('/task2B/', (req, res) => {
  const fn = parseFullname(req.query.fullname);
  res.send(fn);
});

app.get('/task2C/', (req, res) => {
  const username = canonize(req.query.username);
  res.send(username);
});

app.get('/task2D/', (req, res) => {
  const color = canonizeColor(req.query.color);
  res.send(color);
});

// function notFound(res) {
//   return res.status(404).send('Not Found');
// }
function sendError(res) {
  return res.status(400).send('Error');
}

app.get('/task2X/', (req, res) => {
  if (req.query.i) {
    const i = +(req.query.i);
    if (i >= 0 && i <= 18) {
      // console.log(req.query.i);
      res.send(blackBox(i).toString());
    }
  } else {
    sendError(res);
  }
});

app.listen(3000, () => {
  console.log('Your app listening on port 3000!');
});
