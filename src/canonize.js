export default function canonize(url) {
  const re = new RegExp('(https?:)?(\/\/)?(www.)?(([a-zA-Z]*)[^\/]*\/)?@?([@a-zA-Z0-9._]*)', 'i');
  const username = url.match(re);
  return '@' + username[6];
}