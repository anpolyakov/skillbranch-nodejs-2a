function canonizeHEX(colorString) {
  const re = new RegExp('^#?([0-9abcdef]{3}|[0-9abcdef]{6})$', 'i');
  const color = colorString.match(re);
  if (color == null) {
    return null;
  }
  const result = color[1];
  if (result.length === 3) {
    return result[0] + result[0] + result[1] + result[1] + result[2] + result[2];
  }
  return result.toLowerCase();
}

function canonizeRGB(colorString) {
  const re = new RegExp('^rgb[(]{1} *([12]?[0-9]{1,2}) *, *([12]?[0-9]{1,2}) *, *([12]?[0-9]{1,2}) *[)]{1}$', 'i');
  const color = colorString.match(re);
  if (color == null) {
    return null;
  }
  return printRGB([color[1], color[2], color[3]]);
}

function canonizeHSL(colorString) {
  const re = /^hsl[(]{1} *([123]?[0-9]{1,2}) *, *([1]?[0-9]{1,2})% *, *([1]?[0-9]{1,2})% *[)]{1}$/i;
  const color = colorString.match(re);
  if (color == null) {
      console.log(color);
    return null;
  }
  let h = +color[1];
  if (h > 360) { return null; }
  let s = +color[2];
  if (s > 100) { return null; }
  let l = +color[3];
  if (l > 100) { return null; }
  const rgb = convertHSLtoRGB(h / 360, s / 100, l / 100);
  return printRGB(rgb);
}

function convertHSLtoRGB(h, s, l) {
  let r, g, b;

  if(s == 0) {
    r = g = b = l; // achromatic
  } else {
    let hue2rgb = function hue2rgb(p, q, t){
      if (t < 0) t += 1;
      if (t > 1) t -= 1;
      if (t < 1 / 6) return p + (q - p) * 6 * t;
      if (t < 1 / 2) return q;
      if (t < 2 / 3) return p + (q - p) * (2 / 3 - t) * 6;
      return p;
    }

    let q = l < 0.5 ? l * (1 + s) : l + s - l * s;
    let p = 2 * l - q;
    r = hue2rgb(p, q, h + 1/3);
    g = hue2rgb(p, q, h);
    b = hue2rgb(p, q, h - 1/3);
  }
  return [Math.round(r * 255), Math.round(g * 255), Math.round(b * 255)];
}

function printRGB(rgb) {
  let result = '';
  for (let num = 0; num < 3; num++) {
    const digit = (+rgb[num]).toString(16);
    if (digit.length == 1) {
      result += '0' + digit;
    } else if (digit.length == 2) {
      result += digit;
    } else if (digit.length > 2) {
      return null;
    }
  }
  return result;
}

export default function canonizeColor(url) {
  url = unescape((url + '').trim());

  let result = canonizeHEX(url);
  if (result != null) return '#' + result;

  result = canonizeRGB(url);
  if (result != null) return '#' + result;

  result = canonizeHSL(url);
  if (result != null) return '#' + result;

  return 'Invalid color';
}